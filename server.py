import grpc
from concurrent import futures
import time

import operators_pb2
import operators_pb2_grpc

import functions.operations
from multiprocessing import Queue

from custom_requests import get_request as gr, mock_requests

class CalculatorServicer(operators_pb2_grpc.CalculatorServicer):

    # calculator.square_root is exposed here
    # the request and response are of the data type
    # calculator_pb2.Number
    def Calc(self, request, context):
        response = operators_pb2.Number()
        obj = functions.operations.Operations()
        attr = getattr(obj, request.oper)
        response.value = attr(request.value)
        return response

    def get_request(self, request, context):
        mock_requests()
        response = operators_pb2.Operation()
        i = gr()
        response.oper = i[0]
        response.value.extend(i[1])
        return response

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

# use the generated function `add_CalculatorServicer_to_server`
# to add the defined class to the server
operators_pb2_grpc.add_CalculatorServicer_to_server(
        CalculatorServicer(), server)

# listen on port 50051
print('Starting server. Listening on port 50051.')
server.add_insecure_port('[::]:50051')
server.start()

# since server.start() will not block,
# a sleep-loop is added to keep alive
try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)