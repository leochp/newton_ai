from functools import reduce

class Operations(object):
    def _sum(self, l) -> int:
        x = sum(l)
        return x

    def _multiply(self, l) -> int:
        x = reduce(lambda x, y: x*y, l)
        return x

    def _pass(self, l):
        return -1