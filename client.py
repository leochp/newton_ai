import grpc

# import the generated classes
import operators_pb2
import operators_pb2_grpc
import time

# open a gRPC channel
channel = grpc.insecure_channel('localhost:50051')

# create a stub (client)
stub = operators_pb2_grpc.CalculatorStub(channel)

while True:
    req = operators_pb2.GetRequest()
    i = stub.get_request(req)
    print(f"request -> {i.oper} -- {i.value}")

    numbers = operators_pb2.Operation(oper=f"_{i.oper}", value=i.value)

    response = stub.Calc(numbers)
    print(response.value)

    time.sleep(5)