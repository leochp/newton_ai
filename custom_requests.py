from multiprocessing import Queue
import random

q = Queue()
def mock_requests():
    opers = ["sum", "multiply"]
    for _ in range(10):
        q.put(tuple([opers[random.randint(0, 1)], [random.randint(1, 10) for i in range(10)]]))


def get_request():
    if not q.empty():
        return q.get()
    return ("pass", [])